import pandas as pd
from pymongo import MongoClient
import json
import os

def _connect_mongo(host, port, username, password, db):
    """ 选择连接mongo """
    if username and password:
        mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (username, password, host, port, db)
        conn = MongoClient(mongo_uri)
    else:
        conn = MongoClient(host, port)
    return conn
def read_mongo(db="admin", host='192.168.1.88', port=27002, username="read",
               password="bgl94YgGZejAjpcC"):
    # Connect to MongoDB
    db = _connect_mongo(host=host, port=port, username=username, password=password, db=db)
     # 选择库
    _database = db.app_data
    # 选择集合
    collection_str="judgement_wenshu"
    _collection = _database[collection_str]
    # company_list=_database.collection_names()
    # 读取名单
    key_data = pd.read_excel(io="重点企业名单.xls", index_col=None, usecols=[1], header=None, skiprows=[0, 1],
                             names=["key"])
    key_data = key_data.drop_duplicates()
    path = "./{}.json".format(collection_str)

    # 获取关键字
    for key in key_data["key"]:
        query = {"litigant_list": key}
        print(query)
        """读取数据 """
        results = _collection.find_one(query)
        for res in results:

            res["_id"]=str(res["_id"])
            json_data=json.dumps(res, ensure_ascii=False, indent=2)
            print(key,"写入数据")

            # 写入数据
            if os.path.exists(path):
                with open(path, "a")as file:
                    file.write(json_data + "\n")
            else:
                with open(path, "w")as file:
                    file.write(json_data + "\n")

            # with open("./data2.json", "a")as file:
            #     file.write(json_data+"\n")
if __name__ == '__main__':
    df = read_mongo()
