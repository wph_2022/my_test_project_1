import pymongo
from mongo_config.config_file import LOCAL_MONGO_DATA
import pandas as pd
import json
import os
import time


# import threading

def _connect_mongo():
    """ 选择连接mongo """
    host = LOCAL_MONGO_DATA["host"]
    port = LOCAL_MONGO_DATA["port"]
    username = LOCAL_MONGO_DATA["username"]
    password = LOCAL_MONGO_DATA["password"]
    if username and password:
        mongo_uri = 'mongodb://%s:%s@%s:%s' % (username, password, host, port)
        conn = pymongo.MongoClient(mongo_uri)
    else:
        conn = pymongo.MongoClient(host, port)
    return conn


def connect_mongo_basedata():
    # admin认证库
    admin = _connect_mongo().admin
    # 切换库
    _basedata = "app_data"
    basedata = _connect_mongo()[_basedata]

    # 读取库或集合列表形式
    # _database="read"
    # # company_list = basedata.collection_names()
    # for company in company_list:
    #     with open("./collectio_list.txt", "a")as file:
    #         # file.write(company+"\n")
    #         file.write("\""+company+"\""+"\n")
    #     print(company_list)
    # 选择表
    _collection = "enterprise_data_gov"
    collection = basedata[_collection]
    return collection, _collection


print(connect_mongo_basedata())


# 获取名单数据
def get_key_data():
    # 读取名单
    key_data = pd.read_excel(io="./重点企业名单.xls", index_col=None, usecols=[1], header=None, skiprows=[0, 1],
                             names=["key"])
    key_data = key_data.drop_duplicates()
    # key_data = ["江西铜业集团有限公司",
    #             "国网江西省电力公司",
    #             "中国石油化工股份有限公司九江分公司",
    #             "江西中烟工业有限责任公司",
    #             "江西省水投能源发展有限公司",
    #             "江西铜业铜材有限公司"]

    return key_data


def get_data():
    time_start = time.time()
    collection, _collection = connect_mongo_basedata()
    # 文件存放位置
    path = "./{}.json".format(_collection)
    key_data = get_key_data()
    for key in key_data["key"]:
        with open("./key_work_data.txt","a",encoding="utf-8")as file:
            file.write("\""+str(key)+"\""+"\n"+",")
    # for key in key_data:
    #     query = {"company": key}
    #     json_data = collection.find_one(query)
    #
    #     if json_data:
    #         json_data["_id"] = str(json_data["_id"])
    #         json_data = json.dumps(json_data, ensure_ascii=False, indent=2)
    #         # 写入数据
    #         if os.path.exists(path):
    #             with open(path, "a")as file:
    #                 file.write(json_data + "\n")
    #         else:
    #             with open(path, "w")as file:
    #                 file.write(json_data + "\n")
    # time_end = time.time()
    #
    # print('结束！！使用%s秒' % (time_end - time_start))


if __name__ == '__main__':
    # t1 = threading.Thread(target=get_data)
    # t1.setDaemon(True)
    # t1.start()
    # t1.join()
    get_data()
    # pass
